import * as plugins from './smartspawn.plugins.js';

export const packageBase = plugins.path.join(__dirname, '../');
export const typescriptwrapJs = plugins.path.join(packageBase, 'assets/typescriptwrap.js');
