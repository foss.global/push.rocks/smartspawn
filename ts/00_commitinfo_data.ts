/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartspawn',
  version: '3.0.2',
  description: 'smart subprocess handling'
}
